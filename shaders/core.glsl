#version 330

#ifdef VERTEX_SHADER
uniform mat4 mvpMatrix;

in vec3 position;
in vec3 normal;
in vec3 translation;

out vec3 vertex_normal;

void main( )
{
    //gl_Position= mvpMatrix * vec4(position, 1.0);
    gl_Position= mvpMatrix * vec4(position+translation, 1.0);
    vertex_normal = normal;
}
#endif

#ifdef FRAGMENT_SHADER

in vec3 vertex_normal;
out vec4 fragment_color;

void main( )
{
    fragment_color.rgb = abs(vertex_normal);
}
#endif
