#ifndef __Camera__
#define __Camera__


#include "Geometry.h"
#include "Transform.h"


#include <vector>
#include <cmath>


class Camera
{
private :
  gk::Point position;
  gk::Vector direction;
  gk::Vector up;
public :
  Camera(const gk::Point& position = gk::Point(-1080.0,100.0,-8400.0), const gk::Vector& direction = gk::Vector(0.0,0.0,1.0), const gk::Vector& up = gk::Vector(0.0,1.0,0.0));
  
  gk::Point getPosition() const;
  gk::Vector getDirection() const;
  gk::Vector getUp() const;
  
  
  //rotations
  void rotateX(float pas = 0.1);
  void rotateY(float pas = 0.1);
  void rotateZ(float pas = 0.1);
  
  //deplacement
  void forward(float pas = 0.1);
  
  
  void translate(gk::Vector v);
  
  
  gk::Transform transform();
  
};


#endif //__Camera__
