#include "Camera.h"


Camera::Camera(const gk::Point& position, const gk::Vector& direction, const gk::Vector& up)
: position(position), direction(direction), up(up)
{
}
  


gk::Point Camera::getPosition() const
{
  return position;
}

gk::Vector Camera::getDirection() const
{
  return direction;
}

gk::Vector Camera::getUp() const
{
  return up;
}

/******************************************************************************
*				Rotation Camera
******************************************************************************/

void Camera::rotateX ( float pas ) {
    direction.y = cos(pas)*direction.y + direction.z*sin(pas);
    direction.z = -sin(pas)*direction.y + direction.z*cos(pas);


    up.y = cos(pas)*up.y + up.z*sin(pas);
    up.z = -sin(pas)*up.y + up.z*cos(pas);
}

void Camera::rotateY ( float pas ) {
    direction.x = cos(pas)*direction.x + direction.z*sin(pas);
    direction.z = -sin(pas)*direction.x + direction.z*cos(pas);  
}

void Camera::rotateZ ( float pas ) {
    direction.x = cos(pas)*direction.x + direction.y*sin(pas);
    direction.y = -sin(pas)*direction.x + direction.y*cos(pas);
}

/******************************************************************************
*				Deplacement Camera
******************************************************************************/

void Camera::forward( float pas )
{
  	position += direction * pas;
}


void Camera::translate(gk::Vector v)
{
	position += v;
}


gk::Transform Camera::transform()
{
	return gk::LookAt(position, position + direction,up);
}




