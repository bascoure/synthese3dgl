#include "App.h"
#include "Widgets/nvSDLContext.h"

#include "ProgramManager.h"
#include "MeshIO.h"
#include "Mesh.h"

#include "Grid.h"

#include "Geometry.h"

#include "GL/GLTexture.h"
#include "GL/GLQuery.h"
#include "GL/GLBuffer.h"
#include "GL/GLVertexArray.h"

#include "Camera.h"

#include <map>

std::string world_map_filename;

class TP : public gk::App
{
	nv::SdlContext m_widgets;

	gk::GLProgram *m_program;
	gk::GLVertexArray *m_vao;

	gk::GLBuffer *m_vertex_buffer;
	gk::GLBuffer *m_normal_buffer;
	gk::GLBuffer *m_translate_buffer;
	
	std::map<Block*,std::pair<gk::GLBuffer*,int> > m_translate_map_buffer;
	
	gk::GLBuffer *m_index_buffer;

	gk::Transform model;
	//gk::Transform viewRotationX;
	//gk::Transform viewRotationY;
	//gk::Transform viewPosition;
	gk::Transform projection;

	//double rotationX;
	//double rotationY;
	//gk::Vector positionCamera;
	
	Camera cam;

	int m_indices_size;
	int m_dist; //distance d'afffichage (en unité de block)

	World m_world;

	public:
	// creation du contexte openGL et d'une fenetre
	TP( ):gk::App()
	{
		// specifie le type de contexte openGL a creer :
		gk::AppSettings settings;
		settings.setGLVersion(3,3);     // version 3.3
		settings.setGLCoreProfile();      // core profile
		settings.setGLDebugContext();     // version debug pour obtenir les messages d'erreur en cas de probleme

		// cree le contexte et une fenetre
		if(createWindow(512, 512, settings) < 0)
			closeWindow();

		m_widgets.init();
		m_widgets.reshape(windowWidth(), windowHeight());
	}

	~TP( ) {}

	int init( )
	{
		// cree un shader program
		gk::programPath("shaders");
		m_program= gk::createProgram("core.glsl");
		if(m_program == gk::GLProgram::null()){
			std::cerr << "Cannot load the shader program." << std::endl;
			return -1;
		}

		gk::Mesh* mesh = gk::MeshIO::readOBJ("cube.obj");
		if(mesh == NULL){
			std::cerr << "Cannot load the mesh." << std::endl;
			return -1;
		}

		m_vao= gk::createVertexArray();

		m_vertex_buffer= gk::createBuffer(GL_ARRAY_BUFFER, mesh->positions);
		glVertexAttribPointer(m_program->attribute("position"),3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(m_program->attribute("position"));

		m_normal_buffer= gk::createBuffer(GL_ARRAY_BUFFER, mesh->normals);
		glVertexAttribPointer(m_program->attribute("normal"),3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(m_program->attribute("normal"));

		m_index_buffer= gk::createBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indices);
		m_indices_size = mesh->indices.size();

		delete mesh;
	
		if(m_world.loadMap(world_map_filename)){
			std::cerr << "Cannot load the world : " << world_map_filename << std::endl;
			return -1;
		}

		//nettoyage
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

		for(size_t i=0 ; i<m_world.data.size(); i++){
			Map& map = m_world.data[i];
			for(size_t j=0; j<map.data.size() ; j++){
				Region& region = map.data[j];
				for(size_t k=0; k< region.data.size(); k++){
					Block& block = region.data[k];
					
					Gridpoint origin = block.bbox.pMin;
					int ox = origin.x;
					int oy = origin.y;
					int oz = origin.z;
					
					std::vector<gk::Vector> translation;
					for(int x=0; x<16; x++) 
					for(int y=0; y<16; y++) 
					for(int z=0; z<16; z++) 
					{
						if(isVisible(ox+x,oy+y,oz+z,&block)){
							translation.push_back(gk::Vector(x,y,-z));
						}
					}
					gk::GLBuffer *buffer = gk::createBuffer(GL_ARRAY_BUFFER, translation);
					std::pair<gk::GLBuffer*,int> p(buffer,(int)translation.size());
				
					m_translate_map_buffer[&block] = p;
				}
			}
		}
		
		glClearColor(0.6,0.8,1.0,1.0);

		projection = gk::Perspective(90.0,(float)windowWidth()/windowHeight(),0.1,1000);  
		//rotationX = 180.0;
		//rotationY = 0.0;
		//positionCamera = gk::Point(-1360,48,-8736);
		//positionCamera = gk::Point(-1080,100,-8400);
		//viewPosition = gk::Translate(positionCamera);
		//viewRotationX = gk::RotateX(rotationX);
		//viewRotationY = gk::RotateY(rotationY);
		
		m_dist = 5;

		std::cout << "Fin initialisation" << std::endl;

		return 0;
	}

	int quit( )
	{
		return 0;
	}

	void processWindowResize( SDL_WindowEvent& event )
	{
		projection = gk::Perspective(90.0,(float)windowWidth()/windowHeight(),0.1,1000);  
		m_widgets.reshape(event.data1, event.data2);
	}

	void processKeyboardEvent( SDL_KeyboardEvent& event )
	{
		if(event.type == SDL_KEYDOWN){
			switch(event.keysym.sym){
				case SDLK_z:
					cam.forward(1);
					//positionCamera += gk::Vector(0,0,1);
					//viewPosition = gk::Translate(positionCamera);
					break;
				case SDLK_s:
					cam.forward(-1);
					//positionCamera += gk::Vector(0,0,-1);
					//viewPosition = gk::Translate(positionCamera);
					break;
				case SDLK_d:
					//positionCamera += gk::Vector(-1,0,0);
					//viewPosition = gk::Translate(positionCamera);
					break;
				case SDLK_q:
					//positionCamera += gk::Vector(1,0,0);
					//viewPosition = gk::Translate(positionCamera);
					break;
				case SDLK_r:
					cam.translate(gk::Vector(0.0,1.0,0.0));
					//positionCamera += gk::Vector(0,-1,0);
					//viewPosition = gk::Translate(positionCamera);
					break;
				case SDLK_f:
					cam.translate(gk::Vector(0.0,-1.0,0.0));
					//positionCamera += gk::Vector(0,1,0);
					//viewPosition = gk::Translate(positionCamera);
					break;
				case SDLK_UP:
					cam.rotateX(0.1);
					//rotationX += 10.0;
					//viewRotationX = gk::RotateX(rotationX);
					break;
				case SDLK_DOWN:
					cam.rotateX(-0.1);
					//rotationX -= 10.0;
					//viewRotationX = gk::RotateX(rotationX);
					break;
				case SDLK_LEFT:
					cam.rotateY(0.1);
					//rotationY -= 10.0;
					//viewRotationY = gk::RotateY(rotationY);
					break;
				case SDLK_RIGHT:
					cam.rotateY(-0.1);
					//rotationY += 10.0;
					//viewRotationY = gk::RotateY(rotationY);
					break;
				case SDLK_EQUALS:
					m_dist++;
					break;
				case SDLK_MINUS:
					if(m_dist>1){
						m_dist--;
					}
					break;
				default:
					break;


			}
		}
	}

	bool isVisible(int x, int y, int z, Block* block){
		if(block->voxel(Gridpoint(x,y,z)) < 1){
			return false;
		}

		return (block->voxel(Gridpoint(x+1,y,z)) < 1 ||
			block->voxel(Gridpoint(x-1,y,z)) < 1 || 
			block->voxel(Gridpoint(x,y+1,z)) < 1 || 
			block->voxel(Gridpoint(x,y-1,z)) < 1 || 
			block->voxel(Gridpoint(x,y,z+1)) < 1 || 
			block->voxel(Gridpoint(x,y,z-1)) < 1);
	}

	void afficheBlock(int i, int j, int k, gk::Transform& view){
		Gridpoint originBlock(i, j, k);
		Block* block = m_world.block(originBlock);
		if(block != NULL){
			int ox = block->bbox.pMin.x;
			int oy = block->bbox.pMin.y;
			int oz = block->bbox.pMin.z;
			gk::Transform m = gk::Translate(gk::Vector(ox,oy,oz));
			m_program->uniform("mvpMatrix") = (projection * view * m).matrix();
			std::pair<gk::GLBuffer*, int>& p = m_translate_map_buffer[block];

			glBindVertexArray(m_vao->name);
			glBindBuffer(GL_ARRAY_BUFFER,p.first->name);
			glVertexAttribPointer(m_program->attribute("translation"),3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(m_program->attribute("translation"));
			glVertexAttribDivisor(m_program->attribute("translation"),1);
			glDrawElementsInstanced(GL_TRIANGLES,m_indices_size,GL_UNSIGNED_INT,0,p.second);
		}
	}

	int draw( )
	{
		if(key(SDLK_ESCAPE))
			closeWindow();

		if(key('l'))
		{
			key('l')= 0;
			gk::reloadPrograms();
		}

		if(key('p'))
		{
			key('p')=0;
			gk::writeFramebuffer("screenshot.png");
		}

		// redimensionne l'image en fonction de la fenetre de l'application
		glViewport(0, 0, windowWidth(), windowHeight());
		// efface l'image
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// selectionne le shader program a utiliser

		//gk::Transform view = viewRotationX * viewRotationY * viewPosition;
		gk::Transform view = cam.transform();
		glUseProgram(m_program->name);

		gk::Point positionCamera = cam.getPosition();
		for(int i=-m_dist; i<=m_dist; i++){
			for(int j=-m_dist; j<=m_dist; j++){
				for(int k=-m_dist; k<=m_dist; k++){
					int deltaX = i*16;
					int deltaY = j*16;
					int deltaZ = k*16;
					afficheBlock(
						positionCamera.x + deltaX,
						positionCamera.y + deltaY,
						positionCamera.z + deltaZ,view
						);
				}
			}
		}
		// un peu de nettoyage
		glUseProgram(0);
		glBindVertexArray(0);

		// visualise l'image que l'on vient de dessiner.
		present();

		return 1;
	}
};


int main( int argc, char **argv )
{
	//world_map_filename = "/home/etu/t/p1208831/Bureau/TP2_last_version/westeros/winterfell";
	world_map_filename = "/home/etu/f/p0806634/Documents/Synthese_Image/TP2_last_version/westeros/winterfell";
	TP app;
	app.run();

	return 0;
}

